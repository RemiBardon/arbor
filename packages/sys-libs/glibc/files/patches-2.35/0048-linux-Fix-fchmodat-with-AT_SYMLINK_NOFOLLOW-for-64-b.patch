Upstream: yes, taken from release/2.35/master

From 7d96aa2d7dba5a7d9c80334f1860445bd76fdc7b Mon Sep 17 00:00:00 2001
From: Adhemerval Zanella <adhemerval.zanella@linaro.org>
Date: Wed, 27 Apr 2022 13:40:30 -0300
Subject: [PATCH 048/115] linux: Fix fchmodat with AT_SYMLINK_NOFOLLOW for 64
 bit time_t (BZ#29097)

The AT_SYMLINK_NOFOLLOW emulation ues the default 32 bit stat internal
calls, which fails with EOVERFLOW if the file constains timestamps
beyond 2038.

Checked on i686-linux-gnu.

(cherry picked from commit 118a2aee07f64d605b6668cbe195c1f44eac6be6)
---
 NEWS                               |  2 ++
 io/Makefile                        |  8 +++++---
 io/tst-lchmod-time64.c             |  2 ++
 io/tst-lchmod.c                    | 22 +++++++++++++++++++++-
 sysdeps/unix/sysv/linux/fchmodat.c |  4 ++--
 5 files changed, 32 insertions(+), 6 deletions(-)
 create mode 100644 io/tst-lchmod-time64.c

diff --git a/NEWS b/NEWS
index 87224df4c7..5e75481e4b 100644
--- a/NEWS
+++ b/NEWS
@@ -28,6 +28,8 @@ The following bugs are resolved with this release:
     cancellation and with cancellation disabled
   [29062] elf: Fix memory leak in _dl_find_object_update
   [29078] <dlfcn.h> functions unusable during early auditing
+  [29097] time: fchmodat does not handle 64 bit time_t for
+    AT_SYMLINK_NOFOLLOW
 
 
 Version 2.35
diff --git a/io/Makefile b/io/Makefile
index cf265dc9b9..b1710407d0 100644
--- a/io/Makefile
+++ b/io/Makefile
@@ -83,16 +83,17 @@ tests		:= test-utime test-stat test-stat2 test-lfs tst-getcwd \
 		   tst-ftw-bz28126
 
 tests-time64 := \
+  tst-fcntl-time64 \
+  tst-fts-time64 \
   tst-futimens-time64 \
   tst-futimes-time64\
-  tst-fts-time64 \
+  tst-futimesat-time64 \
+  tst-lchmod-time64 \
   tst-lutimes-time64 \
   tst-stat-time64 \
-  tst-futimesat-time64 \
   tst-utime-time64 \
   tst-utimensat-time64 \
   tst-utimes-time64 \
-  tst-fcntl-time64 \
   # tests-time64
 
 # Likewise for statx, but we do not need static linking here.
@@ -136,6 +137,7 @@ CFLAGS-close.c += -fexceptions -fasynchronous-unwind-tables
 
 CFLAGS-test-stat.c += -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE
 CFLAGS-test-lfs.c += -D_LARGEFILE64_SOURCE
+CFLAGS-tst-lchmod.c += -D_FILE_OFFSET_BITS=64
 
 test-stat2-ARGS = Makefile . $(objpfx)test-stat2
 
diff --git a/io/tst-lchmod-time64.c b/io/tst-lchmod-time64.c
new file mode 100644
index 0000000000..f2b7cc9d35
--- /dev/null
+++ b/io/tst-lchmod-time64.c
@@ -0,0 +1,2 @@
+#define CHECK_TIME64
+#include "tst-lchmod.c"
diff --git a/io/tst-lchmod.c b/io/tst-lchmod.c
index c644f565f6..c1c41bda88 100644
--- a/io/tst-lchmod.c
+++ b/io/tst-lchmod.c
@@ -66,10 +66,27 @@ select_path (bool do_relative_path, const char *full_path, const char *relative_
     return full_path;
 }
 
+static void
+update_file_time_to_y2038 (const char *fname, int flags)
+{
+#ifdef CHECK_TIME64
+  /* Y2038 threshold plus 1 second.  */
+  const struct timespec ts[] = { { 0x80000001LL, 0}, { 0x80000001LL } };
+  TEST_VERIFY_EXIT (utimensat (AT_FDCWD, fname, ts, flags) == 0);
+#endif
+}
+
 static void
 test_1 (bool do_relative_path, int (*chmod_func) (int fd, const char *, mode_t, int))
 {
   char *tempdir = support_create_temp_directory ("tst-lchmod-");
+#ifdef CHECK_TIME64
+  if (!support_path_support_time64 (tempdir))
+    {
+      puts ("info: test skipped, filesystem does not support 64 bit time_t");
+      return;
+    }
+#endif
 
   char *path_dangling = xasprintf ("%s/dangling", tempdir);
   char *path_file = xasprintf ("%s/file", tempdir);
@@ -93,9 +110,12 @@ test_1 (bool do_relative_path, int (*chmod_func) (int fd, const char *, mode_t,
   xsymlink ("loop", path_loop);
   xsymlink ("target-does-not-exist", path_dangling);
 
+  update_file_time_to_y2038 (path_file, 0);
+  update_file_time_to_y2038 (path_to_file, AT_SYMLINK_NOFOLLOW);
+
   /* Check that the modes do not collide with what we will use in the
      test.  */
-  struct stat64 st;
+  struct stat st;
   xstat (path_file, &st);
   TEST_VERIFY ((st.st_mode & 0777) != 1);
   xlstat (path_to_file, &st);
diff --git a/sysdeps/unix/sysv/linux/fchmodat.c b/sysdeps/unix/sysv/linux/fchmodat.c
index 7aa073bf3c..7aae02148a 100644
--- a/sysdeps/unix/sysv/linux/fchmodat.c
+++ b/sysdeps/unix/sysv/linux/fchmodat.c
@@ -48,8 +48,8 @@ fchmodat (int fd, const char *file, mode_t mode, int flag)
 
       /* Use fstatat because fstat does not work on O_PATH descriptors
 	 before Linux 3.6.  */
-      struct stat64 st;
-      if (__fstatat64 (pathfd, "", &st, AT_EMPTY_PATH) != 0)
+      struct __stat64_t64 st;
+      if (__fstatat64_time64 (pathfd, "", &st, AT_EMPTY_PATH) != 0)
 	{
 	  __close_nocancel (pathfd);
 	  return -1;
-- 
2.36.1

