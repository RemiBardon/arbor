# Copyright 2018 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require alternatives github [ user='universal-ctags' pn='ctags' tag=p${PV} ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]

SUMMARY="Universal ctags"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    json [[ description = [ Add support for JSON format as output ] ]]
    seccomp [[ description = [ Add support for libseccomp library ] ]]
    xml
    yaml [[ description = [ Add support for YAML format as output ] ]]
"

DEPENDENCIES="
    build:
        dev-python/docutils
        virtual/pkg-config
    build+run:
        json? ( dev-libs/jansson )
        seccomp? ( sys-libs/libseccomp )
        xml? ( dev-libs/libxml2:2.0[>=2.7.7] )
        yaml? ( dev-libs/libyaml )
"

AT_M4DIR=( m4 )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-etags
    --enable-tmpdir=/tmp
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'json'
    'seccomp'
    'xml'
    'yaml'
)

src_install() {
    default

    # alternatives collision
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{ctags,universal-ctags}
    edo mv "${IMAGE}"/usr/share/man/man1/{ctags,universal-ctags}.1

    alternatives_for ctags universal-ctags 1500 \
        /usr/$(exhost --target)/bin/ctags universal-ctags \
        /usr/share/man/man1/ctags.1 universal-ctags.1
}

